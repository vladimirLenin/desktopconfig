

module Custom.Keys where

    -- Custom
import Custom.MyPrompt
import Custom.Variables
import Custom.MyTreeSelect
import Custom.MyScratchpad
    -- Base
import XMonad
import System.Exit
import qualified XMonad.StackSet as W

    -- Data
import Data.Tree

    -- Hooks
import XMonad.Hooks.ManageDocks (ToggleStruts(..))

    -- ManageHook
import XMonad.ManageHook

    -- Actions
import qualified XMonad.Actions.TreeSelect as TS
--import XMonad.Actions.Search

    -- Layout
import XMonad.Layout.ToggleLayouts (ToggleLayout(..))

    -- Prompt
--import XMonad.Prompt
--import XMonad.Prompt.Shell
--import XMonad.Prompt.Window
import XMonad.Prompt.XMonad
import XMonad.Prompt.RunOrRaise
import XMonad.Prompt.Window
--import XMonad.Prompt.Workspace
--import XMonad.Prompt.ConfirmPrompt
--import XMonad.Prompt.AppLauncher as AL

    -- Actions
import XMonad.Actions.WithAll
import XMonad.Actions.SimpleDate
import XMonad.Actions.TagWindows
import qualified XMonad.Actions.Search as S
--import qualified XMonad.Actions.TreeSelect as TS ( treeselectWorkspace )
import XMonad.Actions.CycleWS
--import XMonad.Actions.GridSelect
--import XMonad.Actions.CopyWindow(copy)
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.DynamicProjects( switchProjectPrompt,  shiftToProjectPrompt )

    -- Util
import XMonad.Util.NamedScratchpad

--myIntelligentGoogleEngine = intelligent google
aiGoogle = S.intelligent S.google
--S.Browser = "/run/current-system/sw/bin/brave"
--nixos    = S.searchEngine "nixos" "https://search.nixos.org/packages"
   --defaultFloating ]

-- run stardict, find it by class name, place it in the floating window
-- 1/6 of screen width from the left, 1/6 of screen height
-- from the top, 2/3 of screen width by 2/3 of screen height
--    NS "stardict" "stardict" (className =? "Stardict")
--        (customFloating $ W.RationalRect (0/6) (1/6) (2/3) (2/3)) ,

-- run gvim, find by role, don't float
--    NS "notes" "gvim --role notes ~/notes.txt" (role =? "notes") nonFloating
 --where role = stringProperty "WM_WINDOW_ROLE"
{-
myScratchPads' :: [NamedScratchpad]
myScratchPads' = [ NS "terminal" spawnTerm findTerm manageTerm
                ]
  where
    spawnTerm  = myTerminal ++ " -n scratchpad"
    findTerm   = resource =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
-}
{-
myScratchPads :: [NamedScratchpad]
myScratchPads = [
                  NS "konsole"  "konsole" (className =? "konsole")  (customFloating $ W.RationalRect (0.08) (0.07) (0.85) (0.84)) 
                , NS "firefox"  "firefox" (className =? "Firefox")  (customFloating $ W.RationalRect (0.04) (0.03) (0.93) (0.95)) 
                , NS "spotify"  "spotify" (className =? "Spotify") defaultFloating  --(customFloating $ W.RationalRect (0.08) (0.07) (0.85) (0.84)) ]
                , NS "syllabus"  "okular ~/books/syllabus.pdf" (className =? "Okular") defaultFloating ] --(customFloating $ W.RationalRect (0.08) (0.07) (0.85) (0.84)) ]

myScratchPads' :: [NamedScratchpad]
myScratchPads' = [NS "konsole" "konsole" (className =? "konsole") (customFloating $ W.RationalRect (1/2) (0.9) (0.95 -0.9) (0.95 -0.9)) ]   --where role = stringProperty "WM_WINDOW_ROLE"
-}




myKeys' :: [(String,X())]
myKeys' = [  ("trivial" , spawn "")
      , ("toggleFullScreen", sendMessage (Toggle "Full"))   
      --, ("hide taffybar" ,  sendMessage ToggleStruts)
      , ("konsole" ,  namedScratchpadAction myScratchPads "konsole")
      , ("firefox" ,  namedScratchpadAction myScratchPads "firefox")
      , ("music" ,  namedScratchpadAction myScratchPads "spotify")
      , ("ug2" ,  namedScratchpadAction myScratchPads "syllabus")
      , ("dwindow" , {-addName "Kill One Window" $ -} kill)
      , ("ddesktop" , {- addName "Kill WorkSpace" $ -} removeWorkspace )
    --  , ("kill all windows",  killAll)                   
      , ("google" , S.promptSearch  myXPConfig aiGoogle )
      , ( "help" , treeselectAction tsDefaultConfig)
      , ("appSearch" , S.promptSearch  myXPConfig nixos )
      , ("youtube" , S.promptSearchBrowser myXPConfig "/run/current-system/sw/bin/brave" S.youtube )
      --, ("myShell",  spawn "emacsclient -c -a '' --eval '(eshell)'")             
      --, ("terminal", {-addName "Konsole" $ -} spawn myTerminal )      
      , ("next-ws",{- addName "next ws" $ -} nextWS)
      , ("prev-ws", {- addName "prev ws" $ -} prevWS)
 --     , ("M-2", addName "capture screen" $ spawn "scrot")
      ,  ("eshutdown", spawn "poweroff")
      ,  ("ereboot", spawn "reboot")
      ,  ("elogout", io $ exitWith ExitSuccess)
      ,  ("exrestart" ,spawn "xmonad --recompile ; xmonad --restart")
      ,  ("xmonadPrompt", xmonadPromptC xmonadEdit ultima2XPConfig)
      ,  ("switchPrompt" , switchProjectPrompt  mainPromptXPConfig)--switchXPConfig)
      ,  ("runOrRaisePrompt" , runOrRaisePrompt runOrRaiseXPConfig)
      ,  ("windowPrompt", windowPrompt myXPConfig'  Goto allWindows)
      ,  ("throwWindowPrompt" , shiftToProjectPrompt myXPConfig)     
      ,  ("tutorial" ,  S.selectSearchBrowser "/run/current-system/sw/bin/brave" tutorial)
      ]
{-
treeselectAction :: TS.TSConfig (X ()) -> X ()
treeselectAction a = TS.treeselectAction a
  [
   Node (TS.TSNode "+ Scratchpads" "" (return ()))
   [  Node (TS.TSNode "konsole" "Drop down terminal" ( namedScratchpadAction myScratchPads "konsole"))[] --(spawn "konsole")) []
   , Node (TS.TSNode "firefox" "Drop down browser"   (  namedScratchpadAction myScratchPads "firefox"))[]
   , Node (TS.TSNode "music" "Drop down spotify"   (  namedScratchpadAction myScratchPads "spotify"))[]
   ]
  ]
-}
