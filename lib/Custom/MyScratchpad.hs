
module Custom.MyScratchpad where

import Custom.Variables

    -- Base
import XMonad
import qualified XMonad.StackSet as W

    -- Util
import XMonad.Util.NamedScratchpad

myScratchPads :: [NamedScratchpad]
myScratchPads = [
                  NS "konsole"  "konsole" (className =? "konsole")  (customFloating $ W.RationalRect (0.08) (0.07) (0.85) (0.84)) 
                , NS myBrowser1  myBrowser1 (className =? "Firefox")  (customFloating $ W.RationalRect (0.04) (0.03) (0.93) (0.95)) 
                , NS "spotify"  "spotify" (className =? "Spotify") defaultFloating  --(customFloating $ W.RationalRect (0.08) (0.07) (0.85) (0.84)) ]
                , NS "syllabus"  "okular ~/books/syllabus.pdf" (className =? "Okular") defaultFloating ]
